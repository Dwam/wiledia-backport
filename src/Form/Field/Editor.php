<?php

namespace Wiledia\Backport\Form\Field;

use Wiledia\Backport\Form\Field;

class Editor extends Field
{
    /*protected static $js = [
        '//cdn.ckeditor.com/4.5.10/standard/ckeditor.js',
    ];*/

    public function render()
    {

        $this->script = "
        // Define function to open filemanager window
        var lfm = function(options, cb) {
        var route_prefix = (options && options.prefix) ? options.prefix : '/laravel-filemanager';
        window.open(route_prefix + '?type=' + options.type || 'file', 'FileManager', 'width=900,height=600');
        window.SetUrl = cb;
        };

        // Define LFM summernote button
        var LFMButton = function(context) {
        var ui = $.summernote.ui;
        var button = ui.button({
            contents: '<i class=\'note-icon-picture\'></i> ',
            tooltip: 'Insert image with filemanager',
            click: function() {

            lfm({type: 'image', prefix: '/laravel-filemanager'}, function(lfmItems, path) {
                lfmItems.forEach(function (lfmItem) {
                context.invoke('insertImage', lfmItem.url);
                });
            });

            }
        });
        return button.render();
        };
        
        $('#{$this->id}').summernote({

            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'underline', 'clear']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'video']],
                ['view', ['fullscreen', 'codeview', 'help']],
                ['popovers', ['lfm']]
                  
              ],
              height: '450',
              disableDragAndDrop: false,
              buttons: {
                lfm: LFMButton
              }

        });";
        return parent::render();
    }
}
